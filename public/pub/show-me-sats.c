#include <stdio.h>

int main()
{
	float price,amount,sats,rate;

	printf("Hello. What's the current bitcoin price?\n");

	scanf ("%f",&price);

	printf("What USD amount do you want to see in terms of sats?\n");

	scanf ("%f",&amount);

	sats = (100000000.00 * amount)/price;
	rate = (100000000/price);

	printf("That is %f sats!\n", sats); 

	printf("The exchange rate is 1 USD to %f sats.\n",rate);


	return 0;
}
